# Nombre: Gabriela Antonia Sepúlveda Rojas
# RUT: 20.787.581-3

# Entrada:  Para comenzar, se importa la librería GI (Gobject Instrospection) para liego seleccionar la versión específica a utilizar.

# Desarrollo: Crearemos una ventana y estableceremos el modelo de nuestro proyecto para luego genenrar las 
# columnas que necesitaremos con lo datos vistos "age, sex, BP, Cholesterol, Na_to_k y drug", la cual
# se encuentra en el archivo csv.

# Salida: Podremos ver la ventana con una tabla generada por los datos
# personales que contiene el archivo csv.

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class ventana_ver():
    
    def __init__(self):

        builder = Gtk.Builder()
        builder.add_from_file("guia2.ui")
        self.ventana = builder.get_object("VentanaVer")
        self.ventana.set_title("Ver datos disponibles")
        self.ventana.set_default_size(450, 580)
        self.ventana.show_all()
        

        tree = builder.get_object("Tree")
        

        self.modelo = Gtk.ListStore(str, str, str, str, str, str)
        tree.set_model(model=self.modelo)


        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("AGE",
                                cell,
                                text=0)
        tree.append_column(column)

        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("SEX",
                                cell,
                                text=1)
        tree.append_column(column)

        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("BP",
                                cell,
                                text=2)
        tree.append_column(column)

        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("CHOLESTEROL",
                                cell,
                                text=3)
        tree.append_column(column)

        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Na_to_K",
                                cell,
                                text=4)
        tree.append_column(column)

        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("DRUG",
                                cell,
                                text=5)
        tree.append_column(column)

        
        archivo = open("drug200.csv")
        linea = 1 
        while linea != "":
            linea = archivo.readline()
            if linea == "":
                continue
            substring = linea.strip().split(",")
            edad = substring[0]
            sexo = substring[1]
            bp = substring[2]
            colesterol = substring[3]
            sodio = substring[4]
            droga = substring[5]
            self.modelo.append([edad, sexo, bp, colesterol, sodio, droga])
